Categories:Office
License:MPLv2
Web Site:https://www.libreoffice.org
Source Code:https://gerrit.libreoffice.org/gitweb?p=impress_remote.git;a=summary
Issue Tracker:https://www.libreoffice.org/bugzilla/buglist.cgi?product=LibreOffice&component=Android%20Impress%20Remote
Donate:https://donate.libreoffice.org
FlattrID:256305
Bitcoin:129jj3HiLfj3zCfqoro3sMTdovizXEdo8A

Auto Name:Impress Remote
Summary:Remote for presentations
Description:
Interact with your slideshow presentation from your Android device.

Features:
* Slide previews
* Speaker notes
* Play/Pause presentation
* Set timers

To set up the app with your computer, follow this guide:
[https://wiki.documentfoundation.org/Development/Impress/RemoteHowTo RemoteHowTo]
.

Repo Type:git
Repo:git://gerrit.libreoffice.org/impress_remote

Build:2.0.0,9
    commit=sdremote-2.0.0
    subdir=android/sdremote
    extlibs=android/android-support-v4.jar
    srclibs=1:Support-v7@android-sdk-4.4.2_r1
    prebuild=mv libs/android-support-v4.jar $$Support-v7$$/libs/

Auto Update Mode:None
Update Check Mode:Tags sdremote-[0-9.]*$
Current Version:2.0.0
Current Version Code:9


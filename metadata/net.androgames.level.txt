Categories:Office
License:GPLv3
Web Site:http://avianey.blogspot.com
Source Code:https://github.com/avianey/Level
Issue Tracker:https://github.com/avianey/Level/issues

Auto Name:Bubble
Summary:Level gauge (measure inclination)
Description:
Hold any of the phone's four sides against an object to test it for level or plumb, or lay it down
on a flat surface for a 360° level.

* Calibrate
* Show angle or inclination
* Sound effects
* Install on SD
* Orientation locking
* Roof pitch
.

# Build Version:1.8.0,27,75,init=rm -f build.xml
# Build Version:1.9.0,29,79,init=rm -f build.xml,target=android-11
# Build Version:1.9.2,31,81,target=android-11
# Build Version:1.9.3,32,84,target=android-11
Build:1.9.4,33
    commit=a5e4dd671e
    subdir=Level

Maintainer Notes:
Source code disappeared from https://github.com/avianey/Level.git

and previously, for the commented builds, from https://androgames-sample.googlecode.com/svn/Market-apps/Level/trunk
.

Auto Update Mode:None
Update Check Mode:None
Current Version:1.9.4
Current Version Code:33


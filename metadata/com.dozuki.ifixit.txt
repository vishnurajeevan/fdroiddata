AntiFeatures:UpstreamNonFree
Categories:System
License:GPLv3
Web Site:http://ifixit.org
Source Code:https://github.com/iFixit/iFixitAndroid
Issue Tracker:https://github.com/iFixit/iFixitAndroid/issues

Auto Name:Dozuki
Summary:Gadget repair guide
Description:
The official iFixit app offers native guide viewing using iFixit’s
[http://www.ifixit.com/api/1.0/doc public APIs], integrated web views of the site with Answers and Cart.

v1.2.1+ is built with the non-free Google Analytics library replaced with the free NoAnalytics.
.

Repo Type:git
Repo:https://github.com/iFixit/iFixitAndroid.git

Build:1.1.1,7
    commit=v1.1.1
    subdir=App
    init=cd .. && \
        git submodule init && \
        git submodule update && \
        cd App
    target=android-15
    update=.,../Android-ViewPagerIndicator/library,../AndroidSectionHeaders/library,../ActionBarSherlock/library,../ImageZoomLibrary,../AndroidImageManager/library,../HoloEverywhere/HoloEverywhereLib

Build:1.2.1,9
    commit=dce77248
    subdir=App
    submodules=yes
    target=android-16
    update=.,../Android-ViewPagerIndicator/library,../AndroidSectionHeaders/library,../ActionBarSherlock/library,../ImageZoomLibrary,../AndroidImageManager/library,../HoloEverywhere/library
    srclibs=NoAnalytics@158a4a
    prebuild=rm -f libs/* && \
        echo "android.library.reference.7=$$NoAnalytics$$" >> project.properties

Build:1.2.3,11
    disable=one error v1.2.3
    commit=unknown - see disabled
    subdir=App
    submodules=yes
    update=.,../Android-ViewPagerIndicator/library,../AndroidSectionHeaders/library,../ActionBarSherlock/library,../ImageZoomLibrary,../AndroidImageManager/library,../HoloEverywhere/library
    srclibs=NoAnalytics@158a4a
    prebuild=rm libs/libGoogleAnalytics.jar && \
        mvn clean package -f ../http-request/lib/pom.xml && \
        mv ../http-request/lib/target/http-request-3.1-SNAPSHOT.jar libs/ && \
        echo "android.library.reference.7=$$NoAnalytics$$" >> project.properties

Build:1.3,12
    commit=v1.3
    subdir=App
    submodules=yes
    target=android-17
    update=.,../Android-ViewPagerIndicator/library,../AndroidSectionHeaders/library,../ActionBarSherlock/library,../ImageZoomLibrary,../AndroidImageManager/library,../HoloEverywhere/library
    srclibs=NoAnalytics@158a4a
    prebuild=rm libs/libGoogleAnalytics.jar && \
        mvn clean package -f ../http-request/lib/pom.xml && \
        mv ../http-request/lib/target/http-request-3.1-SNAPSHOT.jar libs/ && \
        echo "android.library.reference.7=$$NoAnalytics$$" >> project.properties

Build:1.3.2,14
    commit=v1.3.2
    subdir=App
    submodules=yes
    target=android-17
    update=.,../Android-ViewPagerIndicator/library,../AndroidSectionHeaders/library,../ActionBarSherlock/library,../ImageZoomLibrary,../AndroidImageManager/library,../HoloEverywhere/library
    srclibs=NoAnalytics@158a4a
    prebuild=rm libs/libGoogleAnalytics.jar && \
        mvn clean package -f ../http-request/lib/pom.xml && \
        mv ../http-request/lib/target/http-request-3.1-SNAPSHOT.jar libs/ && \
        echo "android.library.reference.7=$$NoAnalytics$$" >> project.properties

Build:2.0.1,37
    commit=v2.0.1
    subdir=App
    gradle=ifixit
    srclibs=NoAnalytics@eacdf5bcc9
    prebuild=rm -f libs/* && \
        mv $$NoAnalytics$$/src/com/* src/com/ && \
        echo 'ifixitAppId="com.dozuki.ifixit"' > gradle.properties

Build:2.1.0,39
    disable=not building
    commit=v2.1.0
    subdir=App
    gradle=ifixit
    srclibs=NoAnalytics@eacdf5bcc9
    prebuild=rm -f libs/* && \
        mv $$NoAnalytics$$/src/com/* src/com/ && \
        echo 'ifixitAppId="com.dozuki.ifixit"' > gradle.properties

Build:2.4.0,45
    commit=v2.4.0
    subdir=App
    gradle=ifixit
    srclibs=NoAnalytics@eacdf5bcc9
    prebuild=rm -f libs/* && \
        mv $$NoAnalytics$$/src/com/* src/com/ && \
        echo 'ifixitAppId="com.dozuki.ifixit"' > gradle.properties && \
        echo 'dozukiAppId=""' >> gradle.properties && \
        echo 'accustreamAppId=""' >> gradle.properties

Build:2.4.1,46
    commit=v2.4.1
    subdir=App
    gradle=ifixit
    srclibs=NoAnalytics@eacdf5bcc9
    prebuild=rm -f libs/* && \
        mv $$NoAnalytics$$/src/com/* src/com/ && \
        echo 'ifixitAppId=com.dozuki.ifixit' > gradle.properties && \
        echo 'dozukiAppId=' >> gradle.properties && \
        echo 'accustreamAppId=' >> gradle.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.6.0
Current Version Code:49


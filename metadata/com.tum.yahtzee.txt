Categories:Games
License:GPLv3
Web Site:https://github.com/MichaelE1000/yahtzee4android
Source Code:https://github.com/MichaelE1000/yahtzee4android
Issue Tracker:https://github.com/MichaelE1000/yahtzee4android/issues

Auto Name:Yahtzee
Summary:Yahtzee/Kniffle game
Description:
No description available
.

Repo Type:git
Repo:https://github.com/MichaelE1000/yahtzee4android.git

Build:1.0,1
    commit=aa4443

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

